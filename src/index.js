#!/usr/bin/env node
const program = require('commander')
const helpOptions = require('./lib/core/help/help.js')
const createCommandes = require('./lib/core/commandes/commandes.js')

// 查看版本号
program.version(require('../package.json').version)

// 创建帮助选项
helpOptions()

createCommandes()

// 解析命令行输入
program.parse(process.argv)
