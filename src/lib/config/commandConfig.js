const chalk = require('chalk')
const ora = require('ora')
const {
  PROJECT_NAME,
  DATABASE_TYPE,
  WEB_FRAME,
  NODE_PROJECT_TYPE
} = require('./typeDefine')

// 项目类型名称
const projectTypes = [
  {
    type: 'list',
    message: ` ${chalk.green(`请选择你要创建的项目类型:`)}`,
    name: 'projectType',
    choices: [PROJECT_NAME.VUE3, PROJECT_NAME.REACT, PROJECT_NAME.NODEJS],
    filter: function (val) {
      // 使用filter将回答变为小写
      return val.toLowerCase()
    }
  }
]

// 选择 web 框架
const webFrameTypes = [
  {
    type: 'list',
    message: ` ${chalk.green(`请选您要使用的Web框架：`)}`,
    name: 'webFrameType',
    choices: [WEB_FRAME.KOA, WEB_FRAME.EXPRESS, WEB_FRAME.NO_WEB_FRAME],
    filter: function (val) {
      // 使用filter将回答变为小写
      return val.toLowerCase()
    }
  }
]

// 选择数据库
const dataBaseTypes = [
  {
    type: 'list',
    message: ` ${chalk.green(`请选数据库类型：`)}`,
    name: 'dataBaseType',
    choices: [
      DATABASE_TYPE.MYSQL,
      DATABASE_TYPE.MONGODB,
      DATABASE_TYPE.LOWDB,
      DATABASE_TYPE.NO_DATABASE
    ],
    filter: function (val) {
      // 使用filter将回答变为小写
      return val.toLowerCase()
    }
  }
]

// 选择数据库
const usePuppeteer = [
  {
    type: 'confirm',
    message: ` ${chalk.green(`是否采用 puppeteer 进行爬虫?`)}`,
    name: 'usePuppeteer'
  }
]

// 输入爬虫网址
const spiderLink = [
  {
    type: 'input',
    message: '输入要爬虫的网址：',
    name: 'link',
    default: 'localhost' // 默认值
  }
]

// 选择 Node 项目类型
const nodeProjectTypes = [
  {
    type: 'list',
    message: ` ${chalk.green(`请选择你要创建的nodejs项目类型：`)}`,
    name: 'nodeProjectType',
    choices: [
      NODE_PROJECT_TYPE.BASICS,
      NODE_PROJECT_TYPE.SERVER,
      NODE_PROJECT_TYPE.SPIDER
    ],
    filter: function (val) {
      // 使用filter将回答变为小写
      return val.toLowerCase()
    }
  }
]

module.exports = {
  projectTypes,
  nodeProjectTypes,
  dataBaseTypes,
  webFrameTypes,
  usePuppeteer,
  spiderLink
}
