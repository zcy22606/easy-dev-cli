const { NODE_PROJECT_TYPE } = require('./typeDefine')

let vueRepo = 'direct:https://gitee.com/zcy22606/vue3-project.git'
let nodeRepo = {
  [NODE_PROJECT_TYPE.BASICS]:
    'direct:https://gitee.com/zcy22606/basics-template.git',
  [NODE_PROJECT_TYPE.SERVER]:
    'direct:https://gitee.com/zcy22606/basics-template.git',
  [NODE_PROJECT_TYPE.SPIDER]: ''
}

module.exports = {
  vueRepo,
  nodeRepo
}
