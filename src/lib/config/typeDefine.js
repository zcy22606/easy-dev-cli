const chalk = require('chalk')
const ora = require('ora')

const PROJECT_NAME = {
  VUE3: 'vue3',
  REACT: 'react',
  NODEJS: 'nodejs'
}

const NODE_PROJECT_TYPE = {
  BASICS: 'basics',
  SERVER: 'server',
  SPIDER: 'spider'
}

const DATABASE_TYPE = {
  MYSQL: 'mysql',
  MONGODB: 'mongodb',
  LOWDB: 'lowdb',
  NO_DATABASE: 'No dataBase'
}

const WEB_FRAME = {
  KOA: 'koa',
  EXPRESS: 'express',
  NO_WEB_FRAME: 'No Web Frame'
}

const SERVER_FOLDER = ['app', 'controller', 'middleware', 'router', 'service']

const KOA_PACKAGE = ['koa', 'koa-router', 'koa-bodyparser', 'koa-cors']

const SPIDER_PACKAGE = ['']

const MESSAGE_LIST = {
  GIT_CLONE_VUE: [
    ora(`${chalk.blue(`开始克隆vue3项目到本地...`)}
  `),
    `${chalk.green('项目克隆成功！')}`
  ],
  GIT_CLONE_NODE: [
    ora(`${chalk.blue(`开始克隆Node项目到本地...`)}
  `),
    `${chalk.green('项目克隆成功！')}`
  ],
  GIT_INIT: [
    ora(`${chalk.blue(`开始初始化本地仓库...`)}
  `),
    `${chalk.green('初始化本地仓库完成！')}`
  ],
  NPM_INSTALL: [
    ora(`${chalk.blue(`开始下载项目安装包...`)}
  `),
    `${chalk.green('已成功下载所有项目依赖！')}`
  ],
  NPM_RUN_LINT: [
    ora(`${chalk.blue(`开始格式化项目...`)}
  `),
    `${chalk.green('格式化项目完成！')}`
  ],
  NPM_RUN_SERVER: [
    ora(` ${chalk.blue(`开始启动项目...`)}
  `),
    `${chalk.green(`项目启动成功！`)}
  `
  ],
  CREATE_FOLDER: [
    ora(` ${chalk.blue(`开始创建项目文件夹...`)}
    `),
    `${chalk.green(`创建项目文件夹成功！`)}
      `
  ],
  CREATE_WEB_FRAME: [
    (name) => {
      return [
        ora(`${chalk.blue(`开始下载${name}相关依赖...`)}
    `)
      ]
    },
    (name) => {
      return `${chalk.green(`已成功下载所有${name}依赖！`)}
      `
    }
  ]
  // CREATE_DATABASE: [
  //   (name) => {
  //     return ora(` ${chalk.blue(`开始下载${name}相关依赖...`)}
  //   `)
  //   },
  //   (name) => {
  //     return `${chalk.green(`已成功下载所有${name}依赖！`)}
  //     `
  //   }
  // ]
}

module.exports = {
  PROJECT_NAME,
  DATABASE_TYPE,
  WEB_FRAME,
  NODE_PROJECT_TYPE,
  SERVER_FOLDER,
  KOA_PACKAGE,
  MESSAGE_LIST
}
