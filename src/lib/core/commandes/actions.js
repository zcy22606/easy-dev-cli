// 系统内置模块
// 第三方模块
const inquirer = require('inquirer')
// 本地定义模块
const { projectTypes, nodeProjectTypes } = require('../../config/commandConfig')
const { PROJECT_NAME } = require('../../config/typeDefine')
const { envVerify } = require('../../utils/util')
// const { saveEnv } = require('../utils/createFile')
// 本地工具方法模块
const { cloneVue, cloneNode, cloneReact } = require('./clone')
const { createVuePageAction, createVueComponentAction } = require('./vueAction/vueAction')
const { createReactPageAction } = require('./reactAction/reactAction')

const createProjectAction = async (project) => {
  const res = await inquirer.prompt(projectTypes)
  switch (res.projectType) {
    case PROJECT_NAME.VUE3:
      await cloneVue(project)
      break
    case PROJECT_NAME.REACT:
      await cloneReact()
      break
    case PROJECT_NAME.NODEJS:
      const name = await inquirer.prompt(nodeProjectTypes)
      const result = await cloneNode(project, name.nodeProjectType)
      break
  }
}

const createPageAction = async (pageName, projectType, dest, build = false) => {
  // projectType = Object.keys(projectType).length === 0 ? undefined : projectType
  let env = projectType?.toUpperCase() ?? ['VUE', 'REACT']
  // 如果未指定项目类型或者指定的项目类型不是NODE项目，则验证.env.easy文件
  if (!projectType || (projectType && !['VUE', 'REACT'].includes(env))) {
    env = envVerify(env, null, build)
  }
  switch (env) {
    case 'VUE':
      await createVuePageAction(pageName, dest)
      break
    case 'REACT':
      await createReactPageAction(pageName, dest)
      break
    default:
      console.log('请在Vue或是React项目文件夹下使用命令')
  }
}

const createComponentAction = async (pageName, projectType, dest, build = false) => {
  // projectType = Object.keys(projectType).length === 0 ? undefined : projectType
  let env = projectType?.toUpperCase() ?? ['VUE', 'REACT']
  // 如果未指定项目类型或者指定的项目类型不是NODE项目，则验证.env.easy文件
  if (!projectType || (projectType && !['VUE', 'REACT'].includes(env))) {
    env = envVerify(env, null, build)
  }
  switch (env) {
    case 'VUE':
      await createVueComponentAction(pageName, dest)
      break
    case 'REACT':
      await createReactComponentAction(pageName, dest)
      break
    default:
      console.log('请在Vue或是React项目文件夹下使用命令')
  }
}

module.exports = {
  createProjectAction,
  createPageAction,
  createComponentAction
}
