const { cloneNode } = require('./nodeAction/nodeClone.js')
const { cloneVue } = require('./vueAction/vueClone.js')
const { cloneReact } = require('./reactAction/reactClone.js')

module.exports = {
  cloneNode,
  cloneVue,
  cloneReact
}
