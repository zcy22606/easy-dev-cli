const program = require('commander')
const { createProjectAction } = require('./actions.js')
const { addNodeRouterAction } = require('./nodeAction/nodeAction')
const { createPageAction, createComponentAction } = require('./actions.js')

const createCommandes = () => {
  createProjectCommandes()
  createVueCommandes()
  createNodeCommandes()
}

// 创建项目命令
const createProjectCommandes = () => {
  program.command('create <project>').description('在当前路径下创建Vue3、React或Node项目').action(createProjectAction)
}

// 创建 Vue 命令
const createVueCommandes = () => {
  const options = program.opts()

  // 添加一个组件
  program
    .command('addcpn <cpnName> [projectType]')
    .description('添加一个组件')
    .action((cpnName, projectType) => {
      createComponentAction(cpnName, projectType, options.dest || `src/components/${cpnName.toLowerCase()}`)
    })

  // 添加页面自动生成路由
  program
    .command('addpage <pageName> [projectType]')
    .description('添加一个页面，自动生成路由配置,例如：easydev addpage users [vue] [-d src/views/users]')
    .action((pageName, projectType) => {
      createPageAction(pageName, projectType, options.dest || `src/views/${pageName.toLowerCase()}`)
    })

  // 添加状态管理模块
  program.command('addstore <pageName>').description('添加一个store模块').action()
}

// 创建 Node 命令
const createNodeCommandes = () => {
  const options = program.opts()

  // 添加 node 路由
  program
    .command('addrouter <webframe> <routerName> [projectType]')
    .description('创建koa路由基础配置，包括路由自动注册。例如：easydev addrouter koa user [-d src/router]')
    .action((webframe, routerName, projectType) => {
      addNodeRouterAction(webframe, routerName, projectType, options.dest || `./src/router/`)
    })
}
module.exports = createCommandes
