// 系统内置模块
const { promisify } = require('util')
const path = require('path')

// 第三方模块
const download = promisify(require('download-git-repo'))
const inquirer = require('inquirer')
const open = require('open')

// 本地定义模块
const { dataBaseTypes, webFrameTypes, usePuppeteer, spiderLink } = require('../../../config/commandConfig')
const { NODE_PROJECT_TYPE, MESSAGE_LIST, SERVER_FOLDER } = require('../../../config/typeDefine')
const { nodeRepo } = require('../../../config/repositoryConfig')

// 本地工具方法模块
const { commandSpawn } = require('../../../utils/terminal')
const { createSpiderProject, createWebFrameConfig, createWebFrame, createDatabase } = require('./nodeCreate')
const { mkdirSync } = require('../../../utils/createFile')

const npm = process.platform === 'win32' ? 'npm.cmd' : 'npm'
const git = 'git'

// 克隆 Node 项目
const cloneNode = async (project, projectType) => {
  switch (projectType) {
    // Node 基础 项目
    case NODE_PROJECT_TYPE.BASICS:
      await download(nodeRepo[NODE_PROJECT_TYPE.BASICS], project, {
        clone: true
      })
      break
    // Node 服务项目
    case NODE_PROJECT_TYPE.SERVER:
      // 选择的框架类型（koa，express）
      const webFrameType = await inquirer.prompt(webFrameTypes)
      // 选择的数据库类型
      let serverDataBaseType = await inquirer.prompt(dataBaseTypes)
      // 开始创建项目
      await createNodeServer(project, webFrameType.webFrameType, serverDataBaseType)
      break
    case NODE_PROJECT_TYPE.SPIDER:
      // 是否采用 puppeteer 爬虫
      const spaderType = await inquirer.prompt(usePuppeteer)
      // // 选择的数据库类型
      const spiderDataBaseType = await inquirer.prompt(dataBaseTypes)
      console.log(spiderDataBaseType)
      // // 爬虫的网址
      const link = await inquirer.prompt(spiderLink)
      console.log(link)
      // // 克隆项目
      await createSpiderProject(spaderType.usePuppeteer, spiderDataBaseType.dataBaseTypes, link.link)
    // break
  }
}

const createNodeServer = async (project, webFrameType, dataBaseType) => {
  // 1.克隆 Node 项目到本地
  MESSAGE_LIST.GIT_CLONE_NODE[0].start()
  await download(nodeRepo[NODE_PROJECT_TYPE.BASICS], project, { clone: true })
  MESSAGE_LIST.GIT_CLONE_NODE[0].succeed(MESSAGE_LIST.GIT_CLONE_NODE[1])

  // 2.初始化 git 仓库
  MESSAGE_LIST.GIT_INIT[0].start()
  await commandSpawn(git, ['init'], { cwd: `./${project}` })
  MESSAGE_LIST.GIT_INIT[0].succeed(MESSAGE_LIST.GIT_INIT[1])

  // 执行 npm install 下载项目依赖
  MESSAGE_LIST.NPM_INSTALL[0].start()
  await commandSpawn(npm, ['install'], { cwd: `./${project}` })
  MESSAGE_LIST.NPM_INSTALL[0].succeed(MESSAGE_LIST.NPM_INSTALL[1])

  // 创建框架
  await createWebFrame(project, npm, webFrameType)

  // 创建相关文件夹
  MESSAGE_LIST.CREATE_FOLDER[0].start()
  // await commandSpawn('mkdir', [...SERVER_FOLDER], {
  //   cwd: `./${project}/src`
  // })
  await mkdirSync([...SERVER_FOLDER], path.resolve(process.cwd(), `./${project}/src`))
  MESSAGE_LIST.CREATE_FOLDER[0].succeed(MESSAGE_LIST.CREATE_FOLDER[1])

  // 创建 web 框架配置文件
  await createWebFrameConfig(webFrameType, project)

  // 开启服务
  await commandSpawn(npm, ['start'], {
    cwd: `./${project}`
  })

  // 打开浏览器默认路由
  open('http://localhost:8000/basic')

  // 创建数据库
  // await createDatabase(project, npm, dataBaseType)
}

module.exports = { cloneNode }
