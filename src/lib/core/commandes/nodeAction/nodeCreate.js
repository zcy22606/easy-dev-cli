// 系统内置模块
const fs = require('fs')
const path = require('path')

// 第三方模块
const dotenv = require('dotenv')

// 本地定义模块
const { WEB_FRAME, DATABASE_TYPE, KOA_PACKAGE, MESSAGE_LIST } = require('../../../config/typeDefine')

// 本地工具方法模块
const { commandSpawn } = require('../../../utils/terminal')
const { ejsCompile, writeFile, mkdirSync } = require('../../../utils/createFile')
const { envVerify } = require('../../../utils/util')

const createSpiderProject = async (usePuppeteer, dataBaseType, link = 'localhost') => {
  if (usePuppeteer === 'Yes') {
    console.log('使用')
  }

  // 创建数据库
  await createDatabase(project, npm, dataBaseType)
}

// 创建 web 框架
const createWebFrame = async (project, npm, webFrameType) => {
  if (webFrameType === WEB_FRAME.KOA) {
    // 下载koa项目依赖包
    const creteWebFrame = MESSAGE_LIST.CREATE_WEB_FRAME[0](webFrameType)[0]
    creteWebFrame.start()
    await commandSpawn(npm, ['install', ...KOA_PACKAGE], {
      cwd: `./${project}`
    })
    // MESSAGE_LIST.CREATE_WEB_FRAME[0](webFrameType)[0].succeed(
    creteWebFrame.succeed(MESSAGE_LIST.CREATE_WEB_FRAME[1](webFrameType))
  } else if (webFrameType.webFrameType === WEB_FRAME.EXPRESS) {
    console.log('express')
  }
}

// 创建 web 框架配置
const createWebFrameConfig = async (webFrameType, project) => {
  await createAppConfig(webFrameType, project)
  await addNodeRouterAction(webFrameType, 'basic', 'NODE', `./${project}/src/router/`, project, true)
  // await createKoaController('basic', `./${project}/src/router`)
  // await createKoaMiddleware('basic', `./${project}/src/router`)
}

// 创建数据库连接文件
const createDatabase = async (project, npm, dataBaseType) => {
  if (dataBaseType.dataBaseType === DATABASE_TYPE.MYSQL) {
    await commandSpawn(npm, ['install', 'mysql2'], {
      cwd: `./${project}`
    })
  } else if (dataBaseType.dataBaseType === DATABASE_TYPE.MONGODB) {
    await commandSpawn(npm, ['install', 'mongoose'], {
      cwd: `./${project}`
    })
  }
}

const handleEjsToFile = async (name, dest, templatePath, fileName) => {
  if (!name) throw new Error(`错误名称是必须的！`)
  // 查询文件是否存在
  const filePath = dest + fileName
  fs.stat(filePath, (err, stats) => {
    if (!err) {
      if (stats) {
        throw new Error(`要创建的${filePath}已存在，请检查名称或路径是否正确！`)
      }
    }
  })
  // 已存在抛出异常, 不存在则创建
  await mkdirSync(dest)
  // 创建路由文件
  const template = await ejsCompile(templatePath, {
    name,
    lowerName: name.toLowerCase(),
    firstUpper: name.substring(0, 1).toUpperCase() + name.substring(1)
  })
  await writeFile(filePath, template)
}

const addNodeRouterAction = async (type, routerName, projectType, dest, project, build = false) => {
  let env = projectType?.toUpperCase() ?? 'NODE'
  // 如果未指定项目类型或者指定的项目类型不是NODE项目，则验证.env.easy文件
  if (!projectType || projectType !== 'NODE') envVerify(env, project, build)
  // 判断 router 文件夹下 index.ts 文件是否存在，不存在则添加，存在则忽略
  if (build) {
    // 创建 router 文件夹下的index.js
    const routerIndexJsPath = path.resolve(__dirname, `../../../template/node/${type.toLowerCase()}/router/router-index.ejs`)
    const routerIndexJs = `./${project}/src/router/index.js`
    await createStaticConfig(routerIndexJsPath, routerIndexJs)
  }

  const routerPath = path.resolve(__dirname, `../../../template/node/${type.toLowerCase()}/router/router.ejs`)
  const middlewarePath = path.resolve(__dirname, `../../../template/node/${type.toLowerCase()}/middleware/middleware.ejs`)
  const controllerPath = path.resolve(__dirname, `../../../template/node/${type.toLowerCase()}/controller/controller.ejs`)
  const routerFileName = `${routerName}.router.js`
  const middlewareFileName = `${routerName}.middleware.js`
  const controllerFileName = `${routerName}.controller.js`
  const middlewareDest = dest.indexOf('router') === -1 ? './src/middleware/' : dest.replace('router', 'middleware')
  const controllerDest = dest.indexOf('router') === -1 ? './src/controller/' : dest.replace('router', 'controller')
  await handleEjsToFile(routerName, dest, routerPath, routerFileName)
  await handleEjsToFile(routerName, middlewareDest, middlewarePath, middlewareFileName)
  await handleEjsToFile(routerName, controllerDest, controllerPath, controllerFileName)

  console.log('添加路由配置成功！')
}

const createAppConfig = async (type, project) => {
  let indexJsPath = path.resolve(__dirname, `../../../template/node/${type.toLowerCase()}/index.ejs`)
  let appIndexJsPath = path.resolve(__dirname, `../../../template/node/${type.toLowerCase()}/app/app-index.ejs`)

  const indexJs = `./${project}/src/index.js`
  const appIndexJs = `./${project}/src/app/index.js`
  await createStaticConfig(indexJsPath, indexJs)
  await createStaticConfig(appIndexJsPath, appIndexJs)
}

const createStaticConfig = async (templatePath, fileName) => {
  const template = await ejsCompile(templatePath)
  await writeFile(fileName, template)
}

module.exports = {
  createSpiderProject,
  createWebFrame,
  createWebFrameConfig,
  createDatabase,
  addNodeRouterAction,
  createAppConfig
}
