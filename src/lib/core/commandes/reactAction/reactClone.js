// 系统内置模块
const { promisify } = require('util')

// 第三方模块
const download = promisify(require('download-git-repo'))
const inquirer = require('inquirer')
const open = require('open')

// 本地定义模块
const { dataBaseTypes, webFrameTypes, usePuppeteer, spiderLink } = require('../../../config/commandConfig')
const { NODE_PROJECT_TYPE, MESSAGE_LIST, SERVER_FOLDER } = require('../../../config/typeDefine')
const { reactRepo } = require('../../../config/repositoryConfig')

// 本地工具方法模块
const { commandSpawn } = require('../../../utils/terminal')
const { createSpiderProject, createWebFrame, createDatabase } = require('./reactCreate.js')

const npm = process.platform === 'win32' ? 'npm.cmd' : 'npm'
const git = 'git'

const cloneReact = () => {}

module.exports = { cloneReact }
