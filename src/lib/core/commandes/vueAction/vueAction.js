const { createVuePageAction, createVueComponentAction } = require('./vueCreate')

module.exports = {
  createVuePageAction,
  createVueComponentAction
}
