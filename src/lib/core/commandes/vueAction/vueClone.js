// 系统内置模块
const { promisify } = require('util')

// 第三方模块
const download = promisify(require('download-git-repo'))
const inquirer = require('inquirer')
const open = require('open')

// 本地定义模块
const { MESSAGE_LIST } = require('../../../config/typeDefine')
const { vueRepo } = require('../../../config/repositoryConfig')

// 本地工具方法模块
const { commandSpawn } = require('../../../utils/terminal')
const npm = process.platform === 'win32' ? 'npm.cmd' : 'npm'
const git = 'git'

// 克隆 Vue 项目
const cloneVue = async (project) => {
  // 1. clone项目
  MESSAGE_LIST.GIT_CLONE_VUE[0].start()
  const res = await download(vueRepo, project, { clone: true })
  MESSAGE_LIST.GIT_CLONE_VUE[0].succeed(MESSAGE_LIST.GIT_CLONE_VUE[1])

  // 初始化 git 仓库
  MESSAGE_LIST.GIT_INIT[0].start()
  await commandSpawn(git, ['init'], { cwd: `./${project}` })
  MESSAGE_LIST.GIT_INIT[0].succeed(MESSAGE_LIST.GIT_INIT[1])

  // 3. 执行 npm install 下载项目依赖
  MESSAGE_LIST.NPM_INSTALL[0].start()
  await commandSpawn(npm, ['install'], { cwd: `./${project}` })
  MESSAGE_LIST.NPM_INSTALL[0].succeed(MESSAGE_LIST.NPM_INSTALL[1])

  // 4.先格式化项目一次，防止 eslint 报错
  MESSAGE_LIST.NPM_RUN_LINT[0].start()
  await commandSpawn(npm, ['run', 'lint'], { cwd: `./${project}` })
  MESSAGE_LIST.NPM_RUN_LINT[0].succeed(MESSAGE_LIST.NPM_RUN_LINT[1])

  // 5.执行 npm run serve
  MESSAGE_LIST.NPM_RUN_SERVER[0].start()
  MESSAGE_LIST.NPM_RUN_SERVER[0].stop()
  await commandSpawn(npm, ['run', 'serve'], { cwd: `./${project}` })
}

module.exports = { cloneVue }
