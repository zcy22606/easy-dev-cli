const path = require('path')
const { handleEjsToFile } = require('../../../utils/createFile.js')
const log = require('../../../utils/log.js')
const createVuePageAction = async (pageName, dest) => {
  await createVueComponentAction(pageName, dest)
  await createVueRouter(pageName, dest)
  log.succeed('创建页面成功！')
}

const createVueComponentAction = async (cpnName, dest) => {
  // ejs 模板路径
  const cpnPath = path.resolve(__dirname, '../../../template/vue/vue3/views/component.ejs')
  const fileName = `${cpnName}.vue`
  await handleEjsToFile(cpnName, dest + '/', cpnPath, fileName)
  log.succeed('创建组件成功！')
}

const createVueRouter = async (routerName, dest) => {
  const routerPath = path.resolve(__dirname, '../../../template/vue/vue3/router/router.ejs')
  const routerDest = dest.indexOf('views') === -1 ? `./src/router/${routerName}/` : dest.replace('views', 'router') + '/'
  const cpnDest = dest.indexOf('views') === -1 ? `@/views/${routerName}/${routerName}.vue` : dest.replace('src', '@') + `/${routerName}.vue`
  const router = dest.indexOf('views') === -1 ? `/${routerName}` : dest.replace('src/', '').replace('views', '')
  const fileName = `${routerName}.ts`
  const options = {
    cpnDest,
    router
  }
  await handleEjsToFile(routerName, routerDest, routerPath, fileName, options)
  log.succeed('创建路由配置成功！')
}
module.exports = {
  createVuePageAction,
  createVueComponentAction
}
