const program = require('commander')

// 添加 options
const helpOptions = () => {
  // 添加脚手架描述信息
  program.option(
    '-d --dest <dest>',
    `Scaffolding tools for quickly
               building Vue, React, and Node projects`
  )

  // program.on('--help', () => {
  //   console.log(`

  //   `)
  //   console.log('Other:')
  //   console.log('  other options')
  // })
  program.parse(process.argv)
}

module.exports = helpOptions
