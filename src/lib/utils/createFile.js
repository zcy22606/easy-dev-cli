const fs = require('fs')
const path = require('path')
const ejs = require('ejs')

const saveEnv = (env) => {
  const rootPath = path.resolve(__dirname, '../../.env')
  return fs.promises.writeFile(rootPath, env)
}

const handleEjsToFile = async (name, dest, templatePath, fileName, options) => {
  if (!name) throw new Error(`错误名称是必须的！`)
  // 查询文件是否存在
  const filePath = dest + fileName
  fs.stat(filePath, (err, stats) => {
    if (!err) {
      if (stats) {
        throw new Error(`要创建的${filePath}已存在，请检查名称或路径是否正确！`)
      }
    }
  })
  // 已存在抛出异常, 不存在则创建
  mkdirSync(dest)
  // 创建路由文件
  const template = await ejsCompile(templatePath, {
    name,
    lowerName: name.toLowerCase(),
    firstUpper: name.substring(0, 1).toUpperCase() + name.substring(1),
    cpnDest: options?.cpnDest,
    router: options?.router
  })
  await writeFile(filePath, template)
}

// 解析 ejs 模板
const ejsCompile = (templatePath, data = {}, options = {}) => {
  return new Promise((resolve, reject) => {
    ejs.renderFile(templatePath, { data }, options, (err, data) => {
      if (err) {
        reject(err)
        return
      }
      resolve(data)
    })
  })
}

// 写入文件
const writeFile = (target, content) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(target, content, (err) => {
      if (err) {
        reject(err.message)
      }
      resolve('写入成功')
    })
  })
}

// 创建文件夹
const mkdirSync = async (dirname, dirPath) => {
  if (Array.isArray(dirname) && dirPath) {
    dirname.forEach((dir) => {
      if (fs.existsSync(path.resolve(dirPath, dir))) return true
      fs.mkdirSync(path.resolve(dirPath, dir))
    })
    return true
  } else {
    if (fs.existsSync(dirname)) {
      return true
    } else {
      if (mkdirSync(path.dirname(dirname))) {
        fs.mkdirSync(dirname)
        return true
      }
    }
  }
}

// mkdirSync(['a', 'b', 'c'], './abc/src')

module.exports = {
  saveEnv,
  ejsCompile,
  writeFile,
  mkdirSync,
  handleEjsToFile
}
