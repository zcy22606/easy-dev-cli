const chalk = require('chalk')

const succeed = (...info) => {
  console.log(chalk.green(info))
}

const error = (...info) => {
  console.log(chalk.red(info))
}

const clear = () => {
  console.clear()
}

module.exports = {
  succeed,
  error,
  clear
}
