/**
 * 执行命令行相关的代码
 */
const chalk = require('chalk')
const { spawn } = require('child_process')

const commandSpawn = (...args) => {
  return new Promise((resolve) => {
    // 执行命令
    const childProcess = spawn(...args)
    // 输出信息
    childProcess.stdout.pipe(process.stdout)
    // 输出错误信息
    childProcess.stderr.pipe(process.stderr)
    // 执行完命令关闭进程
    childProcess.on('close', () => {
      resolve()
    })
  })
}

module.exports = {
  commandSpawn
}
