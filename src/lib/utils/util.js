// 第三方模块
const dotenv = require('dotenv')

const envVerify = (env, project, build) => {
  let envPath = build ? `./${project}/.env.easydev` : `./.env.easydev`
  console.log(envPath)
  dotenv.config({
    path: envPath
  })
  // 项目环境不存在
  if (process.env.PROJECT_ENV !== void 0) {
    return process.env.PROJECT_ENV
  } else if ((Array.isArray(env) && !env.includes(process.env.PROJECT_ENV)) || (!Array.isArray(env) && process.env.PROJECT_ENV !== env)) {
    env = !Array.isArray(env) ? env : env.join('|')
    throw new Error(`不是${env}环境下的项目或.env.easydev中的< PROJECT_ENV >配置不正确，请检查.env.easydev中的< PROJECT_ENV >配置或者在命令结尾加上项目类型`)
  } else {
    throw new Error(`不能识别当前环境，请检查项目根目录中是否存在.env.easydev文件，以及确定 < PROJECT_ENV > 的值为 ${env}`)
  }
}

module.exports = {
  envVerify
}
